function onOpen() {
  SpreadsheetApp.getUi()
   .createMenu("Gmail Manager")
   .addItem("Get Emails", "getGmailEmails")
   .addItem("Set Rows Height", "styleRows")
   .addToUi();
}

/* If executing the script in a new sheet that will make a header */
function newSheet(activeSheet) {
  if (activeSheet.getRange('A1').isBlank()) {
    activeSheet.getRange(1, 1, 1, 15).activate();
    activeSheet.getActiveRangeList()
     .setBackground('#cfe2f3')
     .setFontColor('#073763');
    activeSheet.appendRow(["Thread ID", "Message ID", "Date and Time", "Sender Details", "Receiver Details",
                        "CC Details", "BCC Details", "Subject", "Email Body", "Attachments"]);
    var cellRange = activeSheet.getRange('A1:L1');
    var style = SpreadsheetApp.newTextStyle().setFontSize(12).setBold(true).build();
    cellRange.setTextStyle(style);
    activeSheet.setFrozenRows(1);
    activeSheet.autoResizeColumns(1, 10);
  }
}

/** Pulls data from Gmail */
function getGmailEmails() {
  var label = GmailApp.getUserLabelByName("ToExport");
  var label2 = GmailApp.getUserLabelByName("exported");
  var threads = label.getThreads();
  for (var i = threads.length - 1; i >= 0; i--) {
    var messages = threads[i].getMessages();
    for (var j = 0; j < messages.length; j++) {
      var message = messages[j];
      extractDetails(message);
    }
    threads[i].removeLabel(label);
    Logger.log("Moved email from 'toExport' label, id: " + message.getId());
    threads[i].addLabel(label2);
    Logger.log("Moved email to 'exported' label, id: " + message.getId());
  }
}

var folderID = ""; // <=================== PUT ID HERE <===================
/** Writes data pulled from gmail */
function extractDetails(message) {
  Logger.log("Starting extracting email details into spreadsheet");
  var ID = message.getId(); //last message id in a thread
  var threadID = message.getThread().getId(); //gets the thread from current thread in the loop
  var dateTime = message.getDate(); //gets the date & time of the message
  var subjectText = message.getSubject(); //subject of the thread
  var senderDetails = message.getFrom(); //Name and email of the sender
  var receiverDetails = message.getTo(); //Name and email of the receiver
  var CCDetails = message.getCc(); //cc-ed email
  var BCCDetails = message.getBcc();  //bcc-ed email
  var bodyContents = message.getPlainBody(); //message body
  var attachments = message.getAttachments();
  var attachment;
  Logger.log("Number of attachments: " + attachments.length + " in message with id-" + ID);

  if (attachments.length > 0) { //checking if message has attachment
    try {
      Gmail2Drive(ID, attachments);
      for (var k = 0; k < attachments.length; k++) {
        var fileName = attachments[k].getName();
        var files = DriveApp.getFolderById(folderID).getFilesByName(fileName);

        while (files.hasNext()) {
          var file = files.next();
          //Logs all the files with the given name
          Logger.log("Name: " + file.getName() + ", URL: " + file.getUrl());
          if (attachments.length == 1) {
            attachment = "Attachment copied to drive: '" + file.getName() + "', \nFile URl: " + file.getUrl()
              + "\nFolder URL: " + file.getParents().next().getUrl();
          } else if (attachments.length > 1) {
            var attachmentURLs = getAttachmentURLs(attachments);
            attachment = "Attachments copied to drive folder: '" + file.getParents().next().getName() + "', \nFolder URL: "
              + file.getParents().next().getUrl() + ", \nFile URLs: " + getAttachmentURLs(attachments);
          }
        }
      }
      Logger.log("Added file to the drive for message with id-" + ID);
    } catch (err) {
      attachment = "Attachment is already in the drive";
      Logger.log("File already exists in the drive for message with id-" + ID);
      Logger.log(err);
    }
  } else {
    attachment = "n/a";
  }

  var activeSheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  newSheet(activeSheet);

  if (!isDuplicate(activeSheet, ID)) {
    activeSheet.appendRow([
      threadID, ID, dateTime, senderDetails, receiverDetails, CCDetails, BCCDetails, subjectText, bodyContents, attachment
    ]);
    Logger.log("Completed a row for message '" + ID + "'");
  } else {
    Logger.log("Message id-" + ID + " is already in the sheet, skipping it.")
  }
}

/** Should resize rows by specified height */
function styleRows() {
  /* ################## READ ##################
  https://developers.google.com/apps-script/guides/services/advanced#enabling_advanced_services
  To make this function work:
  1. Go to Resources > Advanced Google services and accept terms
  2. Refresh the script editor and go there again
  3. Enable Google Sheets API (toggle to ON) and wait until settings are saved */

  var height = 55; // Please set the height as the unit of pixel.
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var lastRow = ss.getLastRow(); //last row in active sheet which contains text
  var sheet = ss.getActiveSheet(); // or ss.getActiveSheet()
  var resource = {requests: [{
    updateDimensionProperties: {
      properties: {pixelSize: height},
      range: {sheetId: sheet.getSheetId(), startIndex: 2, endIndex: lastRow, dimension: "ROWS"},
      fields: "pixelSize"
    }}]};
  Sheets.Spreadsheets.batchUpdate(resource, ss.getId());
}

//This function checks all Message IDs for duplicates
function isDuplicate(activeSheet, ID) {
  var lastRow = activeSheet.getLastRow();
  var data = activeSheet.getRange("B2:B" + lastRow).getValues(); //Column range for needed value (message id)
  var numRows = activeSheet.getRange("B2:B" + lastRow).getNumRows();
  var duplicate = false;

  for (var i in data) {
    var row = data[i];
    if (row.includes(ID))
     duplicate = true;
  }

  return duplicate;
}

//ID of the folder in google drive files will be copied to.
function Gmail2Drive(threads, attachments) {
  var parentFolder; // destination folder for email attachments
  var root = DriveApp.getRootFolder();

  if (threads.length > 0) {
    parentFolder = getFolder_(folderID);
  }

  for (var k in attachments) {
    var attachment = attachments[k];
    var isDefinedType = checkIfDefinedType_(attachment);
    if (!isDefinedType) continue;
    var attachmentBlob = attachment.copyBlob();
    var attachmentBlobName = attachmentBlob.getName();

    if (DriveApp.getFilesByName(attachmentBlobName).hasNext() === true) {
      Logger.log(attachmentBlobName + " exists in folder!");
      throw ("ERROR: File exists in folder!");
    } else {
      var file = DriveApp.createFile(attachmentBlob);
      file.moveTo(parentFolder);
      root.removeFile(file);
      Logger.log(attachmentBlobName + " moved to folder.");
    }
  }
}

//This function will get all URLs for attachments in folder
function getAttachmentURLs(attachments) {
  var attachmentURLs = [];
  for (var k = 0; k < attachments.length; k++) {
    var fileName = attachments[k].getName();
    attachmentURLs.push(DriveApp.getFilesByName(fileName).next().getUrl());
  }
  var URLs = attachmentURLs.join(", ");

  return URLs;
}

//This function will get the parent folder in Google drive
function getFolder_(id) {
  return DriveApp.getFolderById(id);
}

//getDate n days back. n must be integer
function getDateNDaysBack_(n) {
  n = parseInt(n);
  var date = new Date();
  date.setDate(date.getDate() - n);

  return Utilities.formatDate(date, Session.getScriptTimeZone(), 'yyyy/MM/dd');
}

//this function will check for file extension type and return boolean
function checkIfDefinedType_(attachment) {
  var fileName = attachment.getName();
  var temp = fileName.split('.');
  var fileExtension = temp[temp.length - 1].toLowerCase();
  //Array of file extension which you would like to extract to Drive
  var fileTypesToExtract = [
        'jpg', 'png', 'doc', 'docx', 'txt', 'xml', 'xls', 'xmlx', 'xlsm', 'pdf', 'rtf', 'zip'
    ]; // <========== MODIFY FOR YOU NEEDS <==========

  if (fileTypesToExtract.indexOf(fileExtension) !== -1) return true;
  else return false;
}