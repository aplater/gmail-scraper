# Gmail Scraper
### Description

A script for parsing Gmail using a Google Apps Script and copying messages from threads into spreadsheet and also saving files into Gdrive.

Made by <a href="https://www.linkedin.com/in/morozovartem/">Artem Morozov</a>.